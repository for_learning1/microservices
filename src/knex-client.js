const BlueBird = require('bluebird')
const knex = require('knex')

const createKnexClient = ({ connectionString, migrationsTableName }) => {
    const client = knex(connectionString)

    const migrationOptions = {
        tableName: migrationsTableName || 'knex_migrations'
    }

    return BlueBird.resolve(client.migrate.latest(migrationOptions))
        .then(() => client)
}

module.exports = createKnexClient
