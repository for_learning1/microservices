const { v4: uuid } = require('uuid')

const primeRequestContext = (req, res, next) => {
    req.context = {
        traceId: uuid()
    }

    next()
}

module.exports = primeRequestContext
