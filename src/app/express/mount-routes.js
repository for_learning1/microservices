const mountRoutes = (app, config) => {
    app.use('/', config.homeApp.router)
    app.use('/record-viewings', config.recordViewingsApp.router)
}

module.exports = mountRoutes
