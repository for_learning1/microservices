const express = require('express')

const createActions = ({ messageStore }) => {

    const recordViewings = (traceId, videoId, userId) => {

        const viewedEvent = {
            id: uuid(),
            type: 'VideoViewed',
            metadata: { traceId, userId },
            data: { userId, videoId }
        }
        const streamName = `viewing-${videoId}`

        return messageStore.write(streamName, viewedEvent)
    }

    return {
        recordViewings
    }
}

const createHandlers = ({ actions }) => {

    const handleRecordViewings = (req, res) => {
        return actions
            .recordViewings(req.context.traceId, req.params.videoId)
            .then(() => res.redirect('/'))
    }

    return {
        handleRecordViewings
    }
}

const createRecordViewings = ({ messageStore }) => {
    const actions = createActions({ messageStore })
    const handlers = createHandlers({ actions })

    const router = express.Router()
    router.route('/:videoId').post(handlers.handleRecordViewings)

    return { actions, handlers, router }
}

module.exports = createRecordViewings
